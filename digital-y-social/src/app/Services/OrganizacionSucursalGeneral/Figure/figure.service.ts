import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Generic } from 'src/app/Model/Generic/generic';
import { Service } from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class FigureService extends Service{
  constructor(private http:HttpClient) {
    super();
   }

   public getFigura():Observable<Generic>{
     return this.http.get<Generic>(this.baseUrl+"Figuras");
   }
}
