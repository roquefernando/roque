import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Generic } from 'src/app/Model/generic';
import { environment } from 'src/environments/environment';
import { Service } from '../../service.service';
@Injectable({
  providedIn: 'root'
})
export class EstatesService extends Service {
  
  constructor(private http:HttpClient) {
    super();
   }

   public getEstate():Observable<Generic>{
     return this.http.get<Generic>(this.baseUrl+"");
   }
}
