import { TestBed } from '@angular/core/testing';

import { OrgnizationService } from './orgnization.service';

describe('OrgnizationService', () => {
  let service: OrgnizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrgnizationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
