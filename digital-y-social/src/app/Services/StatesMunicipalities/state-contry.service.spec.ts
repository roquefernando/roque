import { TestBed } from '@angular/core/testing';

import { StateContryService } from './state-contry.service';

describe('StateContryService', () => {
  let service: StateContryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StateContryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
