import { TestBed } from '@angular/core/testing';

import { CatLineOfActionService } from './cat-line-of-action.service';

describe('CatLineOfActionService', () => {
  let service: CatLineOfActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatLineOfActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
