import { TestBed } from '@angular/core/testing';

import { CatAttentionGroupService } from './cat-attention-group.service';

describe('CatAttentionGroupService', () => {
  let service: CatAttentionGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatAttentionGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
