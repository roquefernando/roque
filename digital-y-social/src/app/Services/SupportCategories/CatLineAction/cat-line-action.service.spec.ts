import { TestBed } from '@angular/core/testing';

import { CatLineActionService } from './cat-line-action.service';

describe('CatLineActionService', () => {
  let service: CatLineActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatLineActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
