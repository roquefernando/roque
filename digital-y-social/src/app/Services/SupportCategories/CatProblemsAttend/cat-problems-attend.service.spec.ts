import { TestBed } from '@angular/core/testing';

import { CatProblemsAttendService } from './cat-problems-attend.service';

describe('CatProblemsAttendService', () => {
  let service: CatProblemsAttendService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatProblemsAttendService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
