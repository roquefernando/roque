import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


//COMPONENTES
import { CaptchaComponent } from './Component/Security/captcha/captcha.component';
import { LoginComponent } from './Component/Session/login/login.component';
import { AffiliateRegistrationComponent } from './Component/Session/affiliate-registration/affiliate-registration.component';
import { FooterComponent } from './Component/General/Footer/footer/footer.component';
import { HeaderComponent } from './Component/General/Header/header/header.component';
import { NoticeOfPrivacyComponent } from './Component/General/Modal/notice-of-privacy/notice-of-privacy.component';
import { RecoveryPasswordComponent } from './Component/General/Modal/recovery-password/recovery-password.component';


// MATERIAL
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';

//Principales
import { SesionDigitalSocialComponent } from './Paginas/sesion-digital-social/sesion-digital-social.component';
import { PrincipalComponent } from './Paginas/principal/principal.component';

//Capcha
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';






@NgModule({
  declarations: [
    AppComponent,
    CaptchaComponent,
    LoginComponent,
    AffiliateRegistrationComponent,
    FooterComponent,
    HeaderComponent,
    NoticeOfPrivacyComponent,
    RecoveryPasswordComponent,


    SesionDigitalSocialComponent,
    PrincipalComponent,

  ],
  imports: [
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatListModule,

    ReactiveFormsModule,
    NgxCaptchaModule,


    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
