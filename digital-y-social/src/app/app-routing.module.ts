import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AffiliateRegistrationComponent } from './Component/Session/affiliate-registration/affiliate-registration.component';
import { LoginComponent } from './Component/Session/login/login.component';
import { PrincipalComponent } from './Paginas/principal/principal.component';
import { SesionDigitalSocialComponent } from './Paginas/sesion-digital-social/sesion-digital-social.component';





const routes: Routes = [
{path:"", component: PrincipalComponent},
{path:"registro", component:PrincipalComponent},
{path:"login", component:PrincipalComponent},

/*{path:"principal", component: PrincipalComponent,children:[
  {path:"",component: LoginComponent},
  {path:"login", component:LoginComponent},
  {path:"registro", component:AffiliateRegistrationComponent},
  
]},*/
{path:"Institucion", component: SesionDigitalSocialComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
