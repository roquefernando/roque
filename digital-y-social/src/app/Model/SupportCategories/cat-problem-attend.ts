export interface CatProblematicas {
    id_cat_problem_attend_pk: number;
    nombre_problema: String;
    created_at: Date;
    updated_at: Date;
}
