export interface CatAttencionGroup {
    id_cat_grupo_atencion_pk: number;
    perfil: String;
    created_at: Date;
    updated_at: Date;
}
