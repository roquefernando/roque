export interface Figure {
    id_figure_pk: number;
    nombre_figura: String;
    created_at: Date;
    updated_at: Date;
}
