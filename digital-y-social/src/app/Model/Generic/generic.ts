export interface Generic {
    estado: boolean;
    mensaje: string;
    size: number;
    data: any[];
}
