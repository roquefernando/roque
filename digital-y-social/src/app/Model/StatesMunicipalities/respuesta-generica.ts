export interface RespuestaGenerica {
    estado: boolean;
    mensaje :string;
    size: number;
    data: any[];
}
