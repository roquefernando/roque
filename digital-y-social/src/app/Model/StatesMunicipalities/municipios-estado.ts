export interface MunicipiosEstado {
    id_municipalities_pk: number;
    nombre: String;
    id_estates_fk: number;
    created_at: Date;
    updated_at: Date;
}
 