export interface EstadosRepublica{
    id_country_state_pk: number;
    nombre: string;
    created_at: Date;
    updated_at: Date;
}
