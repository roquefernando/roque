import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { RecoveryPasswordComponent } from '../../General/Modal/recovery-password/recovery-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  private ruta:any;
  private estadoRuta:any;
  constructor(public dialog: MatDialog) {

  }


  ngOnInit(): void {
  }

  openDialog() {
    const dialogRef = this.dialog.open(RecoveryPasswordComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
