import { Component, OnInit } from '@angular/core';
import { NoticeOfPrivacyComponent } from '../../General/Modal/notice-of-privacy/notice-of-privacy.component';
import {MatDialog} from '@angular/material/dialog';
import {FormControl, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { FigureService } from 'src/app/Services/OrganizacionSucursalGeneral/Figure/figure.service';
import { Figure } from 'src/app/Model/OrganizacionSucursalGeneral/figure';
import { EstadosRepublica } from 'src/app/Model/StatesMunicipalities/estados-republica';
import { EstatesService } from 'src/app/Services/OrganizacionSucursalGeneral/Estates/estates.service';
import { StateContryService } from 'src/app/Services/StatesMunicipalities/state-contry.service';

@Component({
  selector: 'app-affiliate-registration',
  templateUrl: './affiliate-registration.component.html',
  styleUrls: ['./affiliate-registration.component.css']
})
export class AffiliateRegistrationComponent implements OnInit {


  hide = true;


  email = new FormControl('', [Validators.required, Validators.email]);
  public figuras:Figure[]=[];
  public estadosMexico: EstadosRepublica[]=[];
  states: string[] = [
    'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
    'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
    'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
    'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
    'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
    'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
  ];

 //  formularioInstitucion: FormGroup;

  constructor(public dialog: MatDialog,private figura_service:FigureService, private estados_mex_service:StateContryService, private _builder:FormBuilder ){

  }


  ngOnInit(): void {
    this.iniciarVariables();
    this.openDialog();

  }

  public iniciarVariables(){
    this.figura_service.getFigura().subscribe(result =>{
      this.figuras=result.data;
    });

    // Consulta los estados de la republica,Queretaro, CMDX , OAXACA ..
    this.estados_mex_service.getState().subscribe(result =>{
      this.estadosMexico=result.data;
    });


  }


  openDialog() {
    const dialogRef = this.dialog.open(NoticeOfPrivacyComponent);

    dialogRef.afterClosed().subscribe(result => {

    });
  }


  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
}

