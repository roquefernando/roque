import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.css']
})
export class CaptchaComponent implements OnInit {

   siteKey:string;
   theme:string;
  constructor() { 
    this.siteKey="6LfinUkcAAAAAI_j99QWzbt2kAMDtjCVGb6mYMCE";
    this.theme="dark light";
  }

  ngOnInit(): void {
  }

}
