import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SesionDigitalSocialComponent } from './sesion-digital-social.component';

describe('SesionDigitalSocialComponent', () => {
  let component: SesionDigitalSocialComponent;
  let fixture: ComponentFixture<SesionDigitalSocialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SesionDigitalSocialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SesionDigitalSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
