import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  public estadoDesing:any;
  constructor(private router:Router) { 
    this.validarDesing();
  }

  ngOnInit(): void {
  }

  validarDesing(){
   
    if(this.router.url=="/" || this.router.url=="/login"){
        this.estadoDesing=true;
    }else{
      this.estadoDesing=false;  
    }
  }
}
