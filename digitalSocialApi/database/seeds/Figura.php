<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Figura extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $figuras=array('A.C.','I.A.P.','I.B.P.','A.B.P.','F.B.P.','I.A.S.P.','S.C.','S.A. de C.V.','S. de R.L. de C.V.');
    
    public function run()
    {
        $i=0;
        while($i<count($this->figuras)){
            DB::table('figures')->insert(['nombre_figura'=>$this->figuras[$i]]);
            $i++;
        }
    }
}
