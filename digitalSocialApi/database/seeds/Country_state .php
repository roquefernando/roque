<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Country_state  extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $arregloEstados= ["Aguascalientes","Baja California","Baja California Sur","Campeche","Coahuila de Zaragoza","Colima","Chiapas","Chihuahua","Ciudad de México","Durango","Guanajuato","Guerrero","Hidalgo","Jalisco","México","Michoacán de Ocampo","Morelos","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro","Quintana Roo","San Luis Potosí","Sinaloa","Sonora","Tabasco","Tamaulipas","Tlaxcala","Veracruz de Ignacio de la Llave","Yucatán","Zacatecas"];
    public function run()
    {
      $i=0;
      while($i<count($this->arregloEstados)){
        DB::table('country_state')->insert(["nombre"=>$this->arregloEstados[$i]]);
        $i=$i+1;
      }
    }
 
}
