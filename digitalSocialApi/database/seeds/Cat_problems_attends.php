<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Cat_problems_attends extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $problemasAtiende=array('Autismo','Alzheimer','Maltrato Infantil','Desarrollo Social','Exclusión Social','Promoción De La Salud','Cultura De Paz','Protección A La Niñez','Neurorehabilitación Infantil','Vulnerabilidad Social','Salud Visual','Violencia Intrafamiliar','Maltrato A La Mujer','Bullying','Acoso Laboral','Trastornos De Personalidad','Suicidio','Trastornos Del Aprendizaje','Enfermería','Síndrome De Down','Rezago Educativo','Trastornos Del Neurodesarrollo','Trastornos Alimenticios','Discapacidad Motriz','Educación Integral','Factores De Riesgo Durante El Embarazo','Acoso Sexual','Anorexia','Bulimia','Alcoholismo','Adicción Al Cigarro','Asistencia Social','Adopción','Pica','Discapacidad Intelectual','Parálisis Cerebral','Drogadicción','Estancias De Día Para Adultos Mayores','Síndrome De Williams','Demencia','Dislexia','Ludopatía','Adicción A La Marihuana/ Mota','Adicción A La Cocaína/ Perico','Adicción Al Crack','Adicción A Las Metanfetaminas/ Cristal','Adicción A Las Tachas/ éxtasis','Adicción A Los Inhalables/ Mona','Adicción A Los Esteroides- Anabólicos','Adicción A La Heroína/ Chiva','Cáncer Infantil','Pobreza');
    public function run()
    {
        $i=0;
        while($i<count($this->problemasAtiende)){
            DB::table('cat_problems_attends')->insert(['nombre_problema'=>$this->problemasAtiende[$i]]);
            $i++;
        }
    }
}
