<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Cat_attencion_group extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private  $gruposAtencion=array('Niños','Jóvenes','Adultos','3era. Edad','Animales','Oscs','Migración','Pueblos originarios y comunidades indígenas','Medio ambiente y animales','Defensa y promoción derechos humanos');
    public function run()
    {
        $i=0;
        while($i<count($this->gruposAtencion)){
            DB::table('cat_attention_groups')->insert(["perfil"=>$this->gruposAtencion[$i]]);
            $i++;
        }
        $i=0;
      //  factory(App\cat_attention_group::class, 10)->create(); de prueba
    }


    
}


