<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Type_organitzations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    private $tipoOrganizacion=array('Soy OSC','Soy Privado');
    public function run()
    {
        $i=0;
        while($i<count($this->tipoOrganizacion)){
            DB::table('type_organitzations')->insert(['nombre_org'=>$this->tipoOrganizacion[$i]]);
            $i++;
        }
    }
}
