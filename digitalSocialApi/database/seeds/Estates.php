<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Estates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $estados=array("Aprovado","Rechazado");
  
    public function run()
    {
        $i=0;
        while($i<count($this->estados)){
            DB::table('estates')->insert(['nombre_estado'=>$this->estados[$i]]);
            $i++;
        }
    }
}
