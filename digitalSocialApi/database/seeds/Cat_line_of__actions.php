<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Cat_line_of__actions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $lineasAccion= array('Asistencia social','Asistencia jurídica','Atención de la salud y cuestiones sanitarias','Apoyo a la alimentación popular','Apoyo en la defensa y promoción de los derechos humanos','Atención a personas con discapacidad','Desarrollo de los pueblos indígenas','Desarrollo comunitario urbano y rural','Defensa de los derechos de los consumidores','Fomento educativo, cultural, artístico, científico y tecnológico','Fomento de acciones para mejorar la economía popular','Fortalecimiento de otras organizaciones de la sociedad civil','Fortalecimiento del tejido social y seguridad ciudadana','Promoción de la participación ciudadana','Promoción de la equidad de género','Promoción del deporte','Protección del medio ambiente','Protección civil','Otras');
  
    public function run()
    {
        $i=0;
        while($i<count($this->lineasAccion)){
            DB::table('cat_line_of_actions')->insert(['nombre_action'=>$this->lineasAccion[$i]]);
            $i++;
        }
         
    }
}
