<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Country_state::class);
        $this->call(Estates::class); 
        $this->call(Municipalities::class);
        $this->call(Figura::class);
        $this->call(Cat_attencion_group::class);
        $this->call(Cat_line_of__actions::class);
        $this->call(Cat_problems_attends::class);
        $this->call(Type_organitzations::class);

        // $this->call(UsersTableSeeder::class);
    }
}
