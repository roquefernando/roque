<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialNetworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_network', function (Blueprint $table) {
            $table->increments('id_social_network_pk');
            $table->string('nombre_red',100)->nullable();
            $table->string('url');
            $table->unsignedInteger('id_organizacion_fk')->nullable();
            $table->unsignedInteger('id_branch_offices_fk')->nullable();
            $table->foreign('id_organizacion_fk')->references('id_organization_pk')->on('organizations')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_branch_offices_fk')->references('id_branch_offices_pk')->on('branch_offices')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_network');
    }
}
