<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id_organization_pk');
            $table->string("logo_directorio",255)->nullable();
            $table->string("rfc",16);
            $table->unsignedInteger('id_type_organization_fk');
            $table->unsignedInteger('id_municipio_fk');
            $table->unsignedInteger('id_figure_fk');
//            $table->unsignedInteger('id_social_network_fk')->nullable();
            $table->unsignedInteger('id_estado_fk');
            $table->unsignedInteger('id_general_information_fk');
            $table->foreign('id_type_organization_fk')->references('id_type_organitzation_pk')->on('type_organitzations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_municipio_fk')->references('id_municipalities_pk')->on('municipalities')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_figure_fk')->references('id_figure_pk')->on('figures')->onUpdate('cascade')->onDelete('cascade');
      //      $table->foreign('id_social_network_fk')->references('id_social_network_pk')->on('social_network')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_estado_fk')->references('id_estates_pk')->on('estates')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_general_information_fk')->references('id_general_informacion_pk')->on('general_information')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }





    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
