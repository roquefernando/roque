<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_information', function (Blueprint $table) {
            $table->increments('id_general_informacion_pk');
            $table->string('nombre',255);
            $table->string('direccion',255);
            $table->string('telefono',12);
            $table->string('codigo_postal',10);
            $table->string('presentacion',10);
            $table->float('num_beneficiarios',10,2)->nullable();
            $table->float('num_empleados',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_information');
    }
}

