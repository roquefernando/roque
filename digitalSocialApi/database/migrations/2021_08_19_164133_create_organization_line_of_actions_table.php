<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationLineOfActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_line_of_actions', function (Blueprint $table) {
            $table->increments('id_organization_line_of_actions_pk');
            $table->unsignedInteger('id_orgnanization_fk')->nullable();
            $table->unsignedInteger('id_branch_offices_fk')->nullable();
            $table->unsignedInteger('id_cat_line_of_action_fk');
            $table->foreign('id_orgnanization_fk')->references('id_organization_pk')->on('organizations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_branch_offices_fk')->references('id_branch_offices_pk')->on('branch_offices')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_cat_line_of_action_fk')->references('id_cat_line_of_action_pk')->on('cat_line_of_actions')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_line_of_actions');
    }
}
