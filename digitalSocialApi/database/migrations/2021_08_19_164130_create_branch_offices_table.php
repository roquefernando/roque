<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_offices', function (Blueprint $table) {
            $table->increments('id_branch_offices_pk');
            $table->unsignedInteger('id_organization_fk');
            $table->unsignedInteger('id_municipio_fk');
       //    $table->unsignedInteger('id_social_network_fk')->nullable();
            $table->unsignedInteger('id_general_information_fk');
            $table->foreign('id_organization_fk')->references('id_organization_pk')->on('organizations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_municipio_fk')->references('id_municipalities_pk')->on('municipalities')->onUpdate('cascade')->onDelete('cascade');
          //  $table->foreign('id_social_network_fk')->references('id_social_network_pk')->on('social_network')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_general_information_fk')->references('id_general_informacion_pk')->on('general_information')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_offices');
    }
}
