<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationProblemsAttendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_problems_attends', function (Blueprint $table) {
            $table->increments('id_organization_problems_attends_pk');
            $table->unsignedInteger('id_organization_fk')->nullable();
            $table->unsignedInteger('id_branch_offices_fk')->nullable();
            $table->unsignedInteger('id_cat_problem_attend_fk');
            $table->foreign('id_cat_problem_attend_fk')->references('id_cat_problem_attend_pk')->on('cat_problems_attends')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_organization_fk')->references('id_organization_pk')->on('organizations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_branch_offices_fk')->references('id_branch_offices_pk')->on('branch_offices')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
           
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_problems_attends');
    }
}
