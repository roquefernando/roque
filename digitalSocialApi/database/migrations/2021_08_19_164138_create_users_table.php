<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id_users_pk');
            $table->string('email')->unique();
            $table->string('password');
            $table->unsignedInteger('id_organizacion_fk')->nullable();
            $table->foreign('id_organizacion_fk')->references('id_organization_pk')->on('organizations')->onUpdate('cascade')->onDelete('cascade');
            $table->string('id_branch_office_fk')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
