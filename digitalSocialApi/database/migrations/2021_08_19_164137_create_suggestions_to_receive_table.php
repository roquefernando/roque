<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestionsToReceiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestions_to_receive', function (Blueprint $table) {
            $table->increments('id_suggestions_to_receive_pk');
            $table->string('nombre_sugerencias',255);
            $table->unsignedInteger('id_organizations_fk')->nullable();
            $table->unsignedInteger('id_branch_offices_fk')->nullable();
            $table->foreign('id_organizations_fk')->references('id_organization_pk')->on('organizations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_branch_offices_fk')->references('id_branch_offices_pk')->on('branch_offices')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggestions_to_receive');
    }
}
