<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id_contacto_pk');
            $table->string('nombre_contacto',255)->nullable();
            $table->string('correo',255);
            $table->unsignedInteger('id_organizacion_fk')->nullable();
            $table->unsignedInteger('id_branch_offices_fk')->nullable();
            $table->foreign('id_organizacion_fk')->references('id_organization_pk')->on('organizations')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_branch_offices_fk')->references('id_branch_offices_pk')->on('branch_offices')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps(); 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
