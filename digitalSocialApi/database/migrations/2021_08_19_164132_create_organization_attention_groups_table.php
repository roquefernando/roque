<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationAttentionGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_attention_groups', function (Blueprint $table) {
            $table->increments('id_organization_attencion_group_pk');
            $table->unsignedInteger('id_orgnanization_fk')->nullable();
            $table->unsignedInteger('id_branch_offices_fk')->nullable();
            $table->unsignedInteger('id_cat_attention_groups_fk');
            $table->foreign('id_orgnanization_fk')->references('id_organization_pk')->on('organizations')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_branch_offices_fk')->references('id_branch_offices_pk')->on('branch_offices')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_cat_attention_groups_fk')->references('id_cat_grupo_atencion_pk')->on('cat_attention_groups')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_attention_groups');
    }
}
