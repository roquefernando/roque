<?php

use Illuminate\Http\Request;
use App\Http\Controllers\pruebaControlle;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['cors'])->group(function () {//ES DE LOCAL
    Route::get('EstadosMexico','Api\General\EstadosRepublicaController@getEstadosRepublica');
    Route::post('MunicipiosEstado','Api\General\EstadosRepublicaController@getMunicipiosEstado');
    Route::get('TipoOrganizacion','Api\Institucion\TypeInstitucion@getTypoInstitucion');    
    
    Route::get('Figuras','Api\General\CategoriasApoyo@getFiguras');
    Route::get('GrupoAtencion','Api\General\CategoriasApoyo@getGruposDeAtencion');
    Route::get('GrupoLineasAccion','Api\General\CategoriasApoyo@getLineasDeAccion');
    Route::get('GrupoProblemasAtiende','Api\General\CategoriasApoyo@getProblemasAtiende');
    
    
});



Route::post('cerrar', 'Api\AuthController@logout')->middleware("auth:api");
Route::post('prueba', "pruebaController@prueba")->middleware("auth:api");
Route::post('registro', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('profile', 'Api\AuthController@profile');
}); 




