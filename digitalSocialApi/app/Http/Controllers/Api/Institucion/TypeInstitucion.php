<?php

namespace App\Http\Controllers\Api\Institucion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\OrganizacionSucursalGeneral\Type_organitzation;
use Exception;

class TypeInstitucion extends Controller
{
    private $TipoOrganizacion;
    

    public function __construct(){

    }
    
    public function getTypoInstitucion(){
        $this->TipoOrganizacion=new Type_organitzation();
        try{
            $this->TipoOrganizacion=new Type_organitzation();
           $this->resultadoApi= responseGeneratorJson(true,"Consulta realizada", count($this->TipoOrganizacion->all()),  $this->TipoOrganizacion->all());
        }catch(Exception $e){
            $this->resultadoApi= responseGeneratorJson(false,"Consulta Fallida","","");
        }
        return $this->resultadoApi;
                

    }
}
