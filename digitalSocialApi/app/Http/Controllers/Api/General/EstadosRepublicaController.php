<?php

namespace App\Http\Controllers\Api\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\EstadosMunicipios\Country_state;
use App\Model\EstadosMunicipios\Municipalitie;
use Validator;
use Exception;

class EstadosRepublicaController extends Controller
{
    private $estadosMunicipios;
    private $municipios;
    private $estadosMexico;
    public function getEstadosRepublica(){
       $this->estadosMexico=new Country_state();
       try{
        $this->resultadoApi=responseGeneratorJson(true,"Consulta realizada", count($this->estadosMexico->all()),$this->estadosMexico->all());
       }catch(Exception $e){
        $this->resultadoApi=responseGeneratorJson(false,"Consulta realizada","","");
       }

       return $this->resultadoApi;
     
      return response()->json($this->resultadoApi);
    }

    public function getMunicipiosEstado(Request $request){
      try{
        $validator = Validator::make($request->all(), [
        'municipio' => 'required|numeric',
       ]);
        $this->municipios=new Municipalitie();
        $datos=$this->municipios->where("id_estates_fk","=",$request->municipio)->get();
        $this->resultadoApi=responseGeneratorJson(true,"Consulta realizada", count($datos),$datos);
      }catch(Exception $e){
        $this->resultadoApi=responseGeneratorJson(false,"Revisa tu formulario","","");
      }
      return response()->json($this->resultadoApi);


    }

}
