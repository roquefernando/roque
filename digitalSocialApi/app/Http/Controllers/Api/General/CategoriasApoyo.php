<?php

namespace App\Http\Controllers\Api\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\OrganizacionSucursalGeneral\Figure;
use App\Model\Categorias_apoyo\Cat_attention_group;
use App\Model\Categorias_apoyo\Cat_line_of_action;
use App\Model\Categorias_apoyo\Cat_problems_attend;
class CategoriasApoyo extends Controller
{
    private $figuras;
    private $grupoAtten;
    private $lineaAccion;
    private $problemasAtiende;
    public function getFiguras(){
        $this->figuras= new Figure();
        $this->resultadoApi=["estado"=>true,"mensaje"=>"Consulta realizada","size"=>count($this->figuras->all()),"data"=>$this->figuras->all()];
        return response()->json($this->resultadoApi);

    }
    public function getGruposDeAtencion(){
        $this->grupoAtten=new Cat_attention_group();
        $this->resultadoApi=["estado"=>true,"mensaje"=>"Consulta realizada","size"=>count($this->grupoAtten->all()),"data"=>$this->grupoAtten->all()];
        return response()->json($this->resultadoApi);

    }
    public function getLineasDeAccion(){
        $this->problemasAtiende=new Cat_line_of_action();
        $this->resultadoApi=["estado"=>true,"mensaje"=>"Consulta realizada","size"=>count($this->problemasAtiende->all()),"data"=>$this->problemasAtiende->all()];
        return response()->json($this->resultadoApi);

    }

    public function getProblemasAtiende(){
        $this->lineaAccion=new Cat_problems_attend();
        $this->resultadoApi=["estado"=>true,"mensaje"=>"Consulta realizada","size"=>count($this->lineaAccion->all()),"data"=>$this->lineaAccion->all()];
        return response()->json($this->resultadoApi);
    }
}
