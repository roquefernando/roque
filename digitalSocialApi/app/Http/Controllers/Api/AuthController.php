<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Sesion\Oauth_Access_Token;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request)
{
    $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required',
     //   'confirm_password' => 'required|same:password',
    ]);

    if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 422);
    }

    $input = $request->all();
    $input['password'] = bcrypt($request->get('password'));
    $user = User::create($input);
    $token =  $user->createToken('MyApp')->accessToken;

    return response()->json([
        'token' => $token,
        'user' => $user
    ], 200);
}

public function login(Request $request)
{
    if (Auth::attempt($request->only('email', 'password'))) {
        $user = Auth::user();
        $tokenSesion= new Oauth_Access_Token();
        $tokenSesion->where('user_id', $user->id_users_pk)->delete();
        $token =  $user->createToken('MyApp')->accessToken;
        return response()->json([
            'token' => $token,
            'user' => $user
        ], 200);
    } else {
        return response()->json(['error' => 'Unauthorised'], 401);
    }
}
public function logout (Request $request) {
    $token = $request->user()->token();
    $token->revoke();
    $token->delete();
    $response = ['message' => 'You have been successfully logged out!'];
    return response($response, 200);
}
public function profile()
{
    $user = Auth::user();
    return response()->json(compact('user'), 200);
}
}