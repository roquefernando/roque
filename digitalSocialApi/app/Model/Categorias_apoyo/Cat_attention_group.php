<?php

namespace App\Model\Categorias_apoyo;

use Illuminate\Database\Eloquent\Model;

class Cat_attention_group extends Model
{
    protected $table = 'cat_attention_groups';
    protected $primaryKey = 'id_cat_grupo_atencion_pk';
}
