<?php

namespace App\Model\Categorias_apoyo;

use Illuminate\Database\Eloquent\Model;

class Cat_problems_attend extends Model
{
    protected $table="cat_problems_attends";
    protected $primaryKey="id_cat_problem_attend_pk";
}
