<?php

namespace App\Model\Categorias_apoyo;

use Illuminate\Database\Eloquent\Model;

class Cat_line_of_action extends Model
{
    protected $table="cat_line_of_actions";
    protected $primaryKey="id_cat_line_of_action_pk";
}
