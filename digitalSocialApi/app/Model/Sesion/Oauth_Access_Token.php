<?php

namespace App\Model\Sesion;

use Illuminate\Database\Eloquent\Model;

class Oauth_Access_Token extends Model
{
    protected $table = 'oauth_access_tokens';
}
