<?php

namespace App\Model\OrganizacionSucursalGeneral;

use Illuminate\Database\Eloquent\Model;

class Figure extends Model
{
    protected $table="figures";
    protected $primaryKey="id_figure_pk";
    
}
