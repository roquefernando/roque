<?php

namespace App\Model\OrganizacionSucursalGeneral;

use Illuminate\Database\Eloquent\Model;

class Type_organitzation extends Model
{
    protected $table="type_organitzations";
    protected $primaryKey="id_type_organitzation_pk";   
}
